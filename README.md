## Installation

Just run `composer install [package_name]`

After that all these functions should be available in every PHP file (via composer's autoloader)

- [mech](#mech)

- [mech_page_open](#mech_page_open)

- [mech_get_page](#mech_get_page)

- [mech_find](#mech_find)

- [mech_find_first](#mech_find_first)

### mech

@var \Behat\Mink\Session $session *

```php
function mech($url = null, $ua = 'desktop')
```


### mech_page_open

@return \Behat\Mink\Element\DocumentElement *

```php
function mech_page_open($url)
```


### mech_get_page

Returns current page

 @return \Behat\Mink\Element\DocumentElement

```php
function mech_get_page()
```


### mech_find

@return \Behat\Mink\Element\Element *

```php
function mech_find($parent, $css)
```


### mech_find_first

Returns the first matching element

```php
function mech_find_first($parent, $css)
```


