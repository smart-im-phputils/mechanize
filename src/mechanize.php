<?php

/** @var \Behat\Mink\Session $session */
global $session;

if (!function_exists('mech')) {
    /**
     * Initializes Mink web crawler (composer require --dev behat/mink behat/mink-goutte-driver)
     *
     * http://mink.behat.org/en/latest/guides/session.html
     * https://packagist.org/packages/behat/mink-goutte-driver
     *
     * @return \Behat\Mink\Session|\Behat\Mink\Element\DocumentElement
     */
    function mech($url = null, $ua = 'desktop') {
        global $session;

        $client = new \Goutte\Client();
        $client->setHeader('user-agent', preg_match('/mob/i', $ua) ? 'Mozilla/5.0 (Linux; U; Android 4.4.2; en-us; SCH-I535 Build/KOT49H) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30' : "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
        $driver = new \Behat\Mink\Driver\GoutteDriver($client);
        $session = new \Behat\Mink\Session($driver);
        $session->start();

        if (!empty($url)) {
            return mech_page_open($url);
        }

        return $session;
    }
}

/** @return \Behat\Mink\Element\DocumentElement */
if (!function_exists('mech_page_open')) {
    /**
     * Visits a url and return page
     */
    function mech_page_open($url) {
        global $session;

        $session->visit($url);
        return $session->getPage();
    }
}

if (!function_exists('mech_get_page')) {
    /**
     * Returns current page
     *
     * @return \Behat\Mink\Element\DocumentElement
     */
    function mech_get_page() {
        global $session;

        return $session->getPage();
    }
}

/** @return \Behat\Mink\Element\Element */
if (!function_exists('mech_find_all')) {
    /**
     * Finds all matching elements in the page
     */
    function mech_find($parent, $css) {
        return $parent->findAll('css', $css);
    }
}

if (!function_exists('mech_find_first')) {
    /**
     * Returns the first matching element
     */
    function mech_find_first($parent, $css) {
        return $parent->find('css', $css);
    }
}